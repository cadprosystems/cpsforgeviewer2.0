const express = require('express');
const path = require('path');

module.exports = function(app){
    // libs
    async function setGetRouter(url, page, controllerName) {
        var args = [];
        for (var i = 0; i < arguments.length; i++) {
            args.push(arguments[i]);
        }
        url = args.shift();
        page = args.shift();
        if (args.length > 0) controllerName = args.shift(); else controllerName = null;
        var ctl = require('./controllers/' + controllerName + '.js');
        var ctlData = await ctl.run();
        var data = Object.assign({}, ctlData);
        app.get(url, (req, res, next) => {
            data = Object.assign(data, req.query);
            res.render(page, data);
        });
    }

    app.get('/', (req, res, next) => {
        res.sendFile(path.join(__dirname + '/../public/index.html'));
    });

    app.get('/test', (req, res, next) => {
        res.send('router testing testing');
    });
    
    setGetRouter('/model:projectId?:token?', 'show-model', 'showModelController');
    setGetRouter('/projectlist', 'project-list', 'projectListController');
    setGetRouter('/uploadfile', 'upload-file', 'projectListController');
}