(function(){

    'use strict';


    function MyExternalExtension(viewer, options) {
        Autodesk.Viewing.Extension.call(this, viewer, options)
    }

    MyExternalExtension.prototype = Object.create(Autodesk.Viewing.Extension.prototype)
    MyExternalExtension.prototype.constructor = MyExternalExtension

    var proto = MyExternalExtension.prototype
    var discardButton = document.getElementById('MyAwesomeLockButton');
    var saveButton = document.getElementById('MyAwesomeUnlockButton');

    var allMarkups;

    axios.get("http://localhost:8080/api/markup/getByModelId/Brushless3.0")
    .then(function (response) {
        console.log("Success!");
        console.log(response);
        allMarkups = response.data;
    })
    .catch(function (error) {
        console.log(error.message);
    });
    

    proto.load = function () {

        console.log('External Extension loaded!')

        var viewer = this.viewer;

        var markup;
        var markupsPersist;
        var viewerStatePersist;
        var controllerGroupOne = createControlGroup(viewer, 'controllerGroupOne');

        var createMarkupButton = createButton('createMarkupButton', 'glyphicon glyphicon-pencil', 'Create Markup', function() {
            console.log('button1 testing');
            viewer.loadExtension('Autodesk.Viewing.MarkupsCore').then(function(markupsExt){
                markup = markupsExt;
                saveButton.style.display = "inline";
                discardButton.style.display = "inline";
                try {
                    //document.getElementById("docking-panel-container-solid-color-b settings-tabs docking-panel-delimiter-shadow").style.display = "background-color: black; width: 200px; height: 200px; left: 50%; top: 50%; position: absolute; z-index: 100000; display: none;";
                } catch (error) {
                    console.log(error);
                }
                

                markup.enterEditMode();
                var cloud = new Autodesk.Viewing.Extensions.Markups.Core.EditModeCloud(markup)
                markup.changeEditMode(cloud)
            });
        });

        /*
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        try {
            let parent_container = document.getElementById("MyViewerDiv");

            var panel = createPanel(parent_container);
            
            panel.addDropDownMenu('markup_scroller', 'Markups', allMarkups, 0, function() {
                continue;
            });
            
            let scroller = panel.createScrollContainer({
                left: false
            });

            scroller.style.cssText = "color: white; height: 100%; width: 100%; position: relative; background-color: black;";

            scroller.addEventListener('click', function() {
                scroller.style.cssText = "background-color: black; width: 200px; height: 200px; left: 50%; top: 50%; position: absolute; z-index: 100000; display: none; color: white;";
                loadMarkup(markup, markupsPersist, viewerStatePersist, saveButton, discardButton);
            });

            let Panel = document.getElementById("docking-panel-container-solid-color-b settings-tabs docking-panel-delimiter-shadow");
            Panel.style.cssText = "background-color: black; width: 200px; height: 200px; left: 50%; top: 50%; position: absolute; z-index: 100000; display: none;";
        }
        catch (error) {
            console.log(error.message, error);
        }
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        */

        var showMarkupButton = createButton('showMarkupButton', 'glyphicon glyphicon-th-list', 'Show Markups', function() {
            console.log('button2 testing');
            try {
                loadMarkup(markup, markupsPersist, viewerStatePersist, saveButton, discardButton);
                saveButton.style.display = "none";
            } catch (error) {
                console.log(error.message);
            }

            // restore the view to the state where the markups were created
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            //let Panel = document.getElementById("docking-panel-container-solid-color-b settings-tabs docking-panel-delimiter-shadow");

            //Panel.style.cssText = "background-color: black; width: 200px; height: 200px; left: 50%; top: 50%; position: absolute; z-index: 100000; display: inline;";
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        });

        // Adds the buttons to the controller groups
        controllerGroupOne.addControl(createMarkupButton);
        controllerGroupOne.addControl(showMarkupButton);
        
        // Event listener for discard button
        discardButton.addEventListener('click', function() {
            // viewer.setNavigationLock(true);
            console.log('discardButton click');
            markup.leaveEditMode()
            markup.hide()
            discardButton.style.display = "none";
            saveButton.style.display = "none";
            //let Panel = document.getElementById("docking-panel-container-solid-color-b settings-tabs docking-panel-delimiter-shadow");
            //Panel.style.cssText = "background-color: black; width: 200px; height: 200px; left: 50%; top: 50%; position: absolute; z-index: 100000; display: none;";
        });

        // Event listener for save button
        saveButton.addEventListener('click', function() {
            //viewer.setNavigationLock(false);
            console.log('saveButton click');
            markupsPersist = markup.generateData()
            viewerStatePersist = markup.viewer.getState()
            //console.log(markupsPersist);
            //console.log(viewerStatePersist);
            
            axios.post('http://localhost:8080/api/markup', {
                markup: markupsPersist,
                viewer_state: JSON.stringify(viewerStatePersist),
                model_id: "Brushless3.0"
            })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error.message);
            });

            axios.get("http://localhost:8080/api/markup/getByModelId/Brushless3.0")
            .then(function (response) {
                console.log("Success!");
                console.log(response.data);
                allMarkups = response.data;
            })
            .catch(function (error) {
                console.log(error.message);
            });

            markup.leaveEditMode()
            markup.hide()
            //console.log(markup);
            saveButton.style.display = "none";
            discardButton.style.display = "none";
            //let Panel = document.getElementById("docking-panel-container-solid-color-b settings-tabs docking-panel-delimiter-shadow");
            //Panel.style.cssText = "background-color: black; width: 200px; height: 200px; left: 50%; top: 50%; position: absolute; z-index: 100000; display: none;";
        });
        return true
    }

    proto.unload = function () {
        // lockBtn.removeEventListener('click', function() {
        //   console.log('lockBtn click been removed');
        // }, false);
    
        // unlockBtn.removeEventListener('click', function() {
        //   console.log('unlockBtn click been removed');
        // }, false);

        console.log('External Extension unloaded! 123')
        return true
    }

    proto.sayHello = function (name) {
        console.log('Hi ' + name + '!')
        return true
    }

    function createButton(id, className, tooltip, handler) {
        var button = new Autodesk.Viewing.UI.Button(id)
        //button.show = false
        button.icon.style.fontSize = '24px'
        button.icon.className = className
        button.setToolTip(tooltip)
        button.onClick = handler
        return button
    }
    /*
    function createPanel(parent_container) {

        var panel = new Autodesk.Viewing.UI.SettingsPanel(
            parent_container,
            "docking-panel-container-solid-color-b settings-tabs docking-panel-delimiter-shadow",
            "Panel",
        );

        panel.addEventListener('click', function() {

        });
        
        return panel;
    }
    */
    function createControlGroup (viewer, ctrlGroupName) {
        var viewerToolbar = viewer.getToolbar(true)

        if (viewerToolbar) {
            var ctrlGroup =  new Autodesk.Viewing.UI.ControlGroup(
            ctrlGroupName)

        viewerToolbar.addControl(ctrlGroup)
        return ctrlGroup
        }
    }

    function loadMarkup(markup, markupsPersist, viewerStatePersist, saveButton, discardButton) {
        new Promise((resolve, reject) => {
            markup.viewer.restoreState(viewerStatePersist, null, true);
            console.log("Round 1");
            resolve();
        })
        .catch((ex) => {
            console.log("Cannot restore state");
            console.log(ex.message);
            reject();
        })
        .then(() => {
            markup.show();
            markup.loadMarkups(markupsPersist, " ");
            console.log("Round 2");

            // saveButton.style.display = "inline";
            discardButton.style.display = "inline";
            console.log("Round 3");
        })
        .catch((ex) => {
            console.log("Cannot show markup");
            console.log(ex.message);
        });
    }

    Autodesk.Viewing.theExtensionManager.registerExtension(
        'MyExternal.Extension.Id',
        MyExternalExtension)
})()