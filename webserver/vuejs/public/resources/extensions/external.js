(function(){

  'use strict';

  function MyExternalExtension(viewer, options) {

    Autodesk.Viewing.Extension.call(this, viewer, options)
  }

  MyExternalExtension.prototype = Object.create(Autodesk.Viewing.Extension.prototype)
  MyExternalExtension.prototype.constructor = MyExternalExtension

  var proto = MyExternalExtension.prototype
  var lockBtn = document.getElementById('MyAwesomeLockButton');
  var unlockBtn = document.getElementById('MyAwesomeUnlockButton');

  proto.load = function () {

    console.log('External Extension loaded!')

    var viewer = this.viewer;
    console.log('viewer', viewer);
    var markup;
    var markupsPersist;
    var viewerStatePersist;
    //viewer.UI.Button('123');
    var controllerGroupOne =  createControlGroup(viewer, 'controllerGroupOne');    
    console.log('controllerGroupOne', controllerGroupOne);
    var button1 = createButton('button1', 'glyphicon glyphicon-envelope', 'Markup', function(){
      console.log('button1 testing');
      // viewer.setNavigationLock(true);
      viewer.loadExtension('Autodesk.Viewing.MarkupsCore').then(function(markupsExt){
        markup = markupsExt;
        // viewer.model.getLayersRoot();
        //console.log(viewer.getLayersRoot());
        markup.enterEditMode();
        var cloud = new Autodesk.Viewing.Extensions.Markups.Core.EditModeCloud(markup)
        markup.changeEditMode(cloud)
      });

    });

    var button2= createButton('button2', 'glyphicon glyphicon-print button-style', 'Show Markup', function(){
      console.log('button2 testing');
      // restore the view to the state where the markups were created
      
      markup.viewer.restoreState(viewerStatePersist);
      // show the markups on a layer
      console.log(markupsPersist);
      // markup.showMarkups(" ");
      // markup.show();
      markup.loadMarkups(markupsPersist, " ");
      //markup.showMarkups(" ");
      // markup.show();

    });
    controllerGroupOne.addControl(button1);
    controllerGroupOne.addControl(button2);
    // console.log('button', button);

    //var lockBtn = document.getElementById('MyAwesomeLockButton');
    lockBtn.addEventListener('click', function() {
      // viewer.setNavigationLock(true);
      console.log('lockBtn click');
      markup.leaveEditMode()
      markup.hide()
    });
  
    //var unlockBtn = document.getElementById('MyAwesomeUnlockButton');
    unlockBtn.addEventListener('click', function() {
      viewer.setNavigationLock(false);
      console.log('unlockBtn click');
      markupsPersist = markup.generateData()
      viewerStatePersist = markup.viewer.getState()
      markup.leaveEditMode()
      markup.hide()
    });

    return true
  }

  proto.unload = function () {
    // lockBtn.removeEventListener('click', function() {
    //   console.log('lockBtn click been removed');
    // }, false);
  
    // unlockBtn.removeEventListener('click', function() {
    //   console.log('unlockBtn click been removed');
    // }, false);

    console.log('External Extension unloaded! 123')

    return true
  }

  proto.sayHello = function (name) {

    console.log('Hi ' + name + '!')

    return true
  }

  function createButton(id, className, tooltip, handler) {

    var button = new Autodesk.Viewing.UI.Button(id)

    button.icon.style.fontSize = '24px'

    button.icon.className = className

    button.setToolTip(tooltip)

    button.onClick = handler

    return button
  }

  function createControlGroup (viewer, ctrlGroupName) {

    var viewerToolbar = viewer.getToolbar(true)

    if (viewerToolbar) {

      var ctrlGroup =  new Autodesk.Viewing.UI.ControlGroup(
        ctrlGroupName)

      viewerToolbar.addControl(ctrlGroup)

      return ctrlGroup
    }
  }

  Autodesk.Viewing.theExtensionManager.registerExtension(
    'MyExternal.Extension.Id',
    MyExternalExtension)
})()