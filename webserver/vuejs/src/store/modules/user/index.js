// import axios from "./../../../services/vue-axios";
// import * as Cookies from "js-cookie";

const defaultState = {
    projectId: ""
};

const getters = {
    getSelectedModel: state => {
        return state.selectedModel;
    }
};

const mutations = {
    addWebToken: function (state, webToken) {},
    removeWebToken: function (state) {},
    setProjectId: function (state, payload) {
        state.projectId = payload.projectId;
    },
    resetState: function (state) {
        // this.state = getDefaultData()
        // store.replaceState(defaultState)
        state.projectId = "";
    }
};

const actions = {
    logout: function (context) {
        // your logout functionality
        context.commit("removeWebToken");
    },
    setProjectId: function (context, payload) {
        // your logout functionality
        context.commit("setProjectId", payload);
    },
    rest: function (context) {
        context.commit("resetState");
    }
};

export default {
    namespaced: true,
    state: defaultState,
    getters,
    actions,
    mutations
};
