import Vue from "vue";
import Router from "vue-router";
import Home from "@/components/Home";
import Projectlist from "@/components/Projectlist";
import Upload from "@/components/Upload";
import Model from "@/components/Model";
import Modal from "@/components/Modal";
import DragnDrop from "@/components/DragnDrop";
Vue.use(Router);

export default new Router({
    routes: [
        {
            path: "/",
            name: "DragnDrop",
            component: DragnDrop
        },
        {
            path: "/projectlist",
            name: "Projectlist",
            component: Projectlist
        },
        {
            path: "/upload",
            name: "Upload",
            component: Upload
        },
        {
            path: "/model",
            name: "Model",
            component: Model
        },
        {
            path: "/modal",
            name: "Modal",
            component: Modal
        }
    ]
});
