"use strict";
const merge = require("webpack-merge");
const prodEnv = require("./prod.env");

// module.exports = merge(prodEnv, {
//   NODE_ENV: '"development"',
//   API_URL: '"http://192.168.99.100:8080/RestApi"',
//   CLIENT_SECRET: '"JInx5GkilhknZ7TNmSOOHfPkQAUZSOhgec97WMMK"',
//   CLIENT_ID: '"2"',
//   SOCKET_URL: "'ws://192.168.99.100:8080/Socket'"
// })

module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    API_URL: '"http://localhost:8080"',
    LOCAL_FORGE_VIEWER: false,
    FORGE_VIEWER_JS_CDN: '"https://developer.api.autodesk.com/modelderivative/v2/viewers/7.*/viewer3D.js"',
    FORGE_VIEWER_CSS_CDN: '"https://developer.api.autodesk.com/modelderivative/v2/viewers/7.*/style.css"',
    FORGE_VIEWER_JS_PATH: '"./static/viewer-3.3/viewer3D.min.js"',
    FORGE_VIEWER_CSS_PATH: '"./static/viewer-3.3/style.min.css"',
});
