const startupDebugger = require("debug")("app:startup");
const consoleLogJoe = require("debug")("coder:joe");
const logger = require("./middlewares/logger");
const authenticate = require("./middlewares/authenticate");
const forge = require("./endpoints/forge");
const markup = require("./endpoints/markup");
const express = require("express");
// const helmet = require("helmet"); // for adding http header
const morgan = require("morgan");
const bodyParser = require("body-parser");
module.exports = function(app) {
    startupDebugger("Api server start...");
    // middleware
    // app.use(helmet());// setting header
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    //preflight
    app.use(function(req, res, next) { 
        //res.setHeader("Access-Control-Allow-Origin", "http://localhost:8080");
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Credentials", "true");
        res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
        res.setHeader(
            "Access-Control-Allow-Headers",
            "Access-Control-Allow-Headers, Access-Control-Allow-Origin, Origin, Accept, X-Requested-With, Content-Type, Authorization"
        );
        console.log("Access-Control-Allow-Origin");
        next();
    });
    // app.options("*",function(req,res,next){
    //     res.header("Access-Control-Allow-Origin", req.get("Origin")||"*");
    //     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    //      //other headers here
    //      console.log('app.options');
    //       res.status(200).end();
    // });

    if (app.get("env") === "development") {
        app.use(morgan("tiny"));// log http requests
        console.log("Morgan enable...");
    }
    //app.use(authenticate);
    //api router
    app.use("/api/forge", forge);
    app.use("/api/markup", markup);
};
