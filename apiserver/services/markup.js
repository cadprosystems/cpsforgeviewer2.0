const config = require("config");
const mongoose = require('mongoose');
//mongoose.connect('mongodb://localhost/my_database');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const MarkupSchema = new Schema({
    markup_id: ObjectId,//???
    markup_data: String,
    viewerstate_data: String,
    model_id: String
});
const mlab = config.get("mlab");
const url = 'mongodb://' + mlab.username + ':' + mlab.password + '@'+ mlab.server + '/' + mlab.db;
module.exports = {
    createTable: async function() {
        // console.log(url);
        const db = await mongoose.createConnection(url, { useNewUrlParser: true });
        await db.createCollection('markups');
        return "OK";
    },
    saveMarkup: async function(markupData, viewerState, modelId) {
        try {
            const db = await mongoose.createConnection(url, { useNewUrlParser: true });
            const Markup = db.model('Markup', MarkupSchema);
            var instance = new Markup();
            //instance.markup_id = mongoose.Types.ObjectId();
            instance.markup_data = markupData;
            instance.viewerstate_data = viewerState;
            instance.model_id = modelId
            const ret = await instance.save();
            db.close();
            return ret;
        } catch (ex) {
            console.log('saveMarkup reject', ex.message);
            return ex.message;
        }
    },
    getMarkup: function(id) {
        return new Promise(async (resolve, reject) => {
            try {
                const db = await mongoose.createConnection(url, { useNewUrlParser: true });
                var Markup = db.model('Markup', MarkupSchema);
                var searchObj = {};
                if(id) {
                    searchObj = {'model_id': id};
                }
                const res = await Markup.find(searchObj);
                db.close();
                resolve(res);
            } catch (ex) {
                console.log('getMarkup reject', ex.message);
                reject(ex.message);
            }
        })
    }
}