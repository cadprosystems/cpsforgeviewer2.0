module.exports = function(req, res, next) {
    const _ = require('underscore')
    , nonSecurePaths = ['/api/forge/', '/about', '/contact'];

    if ( _.contains(nonSecurePaths, req.path) ) return next();
    console.log('authentication middleware');
    //authenticate user
    next();
};