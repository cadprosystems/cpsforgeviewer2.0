const express = require("express");
const router = express.Router();
const Joi = require("joi"); // input data validator
const axios = require("axios");
const config = require("config");
const querystring = require("querystring");
const formidable = require("formidable");
const fs = require("fs");
const path = require("path");
const mzfs = require("mz/fs");
const markupService = require("./../services/markup");
const APS = require('../services/aps.js');

const testingData = [
    { id: 1, name: "Hello World 1" },
    { id: 2, name: "Hello World 2" },
    { id: 3, name: "Hello World 3" }
];

router.get("/", async (req, res) => {
    res.json(testingData);
});
router.get('/auth/token', async function (req, res, next) {
    try {
        res.json(await APS.getPublicToken());
    } catch (err) {
        next(err);
    }
});

router.post("/upload", async function(req, res) {
    try {
        let retObject = {};
        // Read file details
        const result = await readFilesFormPromise(req);
        // console.log(result);
        if (!result.files.file) return res.status(400).send("No files were uploaded.");
        const old_path = path.normalize(result.files.file.path),
            file_size = result.files.file.size,
            file_ext = result.files.file.name.split(".").pop();
        console.log("Old Path: ", old_path);
        var index;
        if (process.platform === "win32") {
            index = old_path.lastIndexOf("\\") + 1;
        } else {
            index = old_path.lastIndexOf("/") + 1;
        }
        console.log("Index: ", index);
        const file_name = result.fields.name + "_" + old_path.substr(index);
        var new_path;
        console.log("1) ", process.cwd());
        console.log("2) ", file_name);
        console.log("3) ", file_ext);
        console.log("Completed path: " + process.cwd() + "\\temp\\" + file_name + "." + file_ext);
        if (process.platform === "win32") {
            new_path = path.join(process.cwd(), "\\temp\\", file_name + "." + file_ext);
        } else {
            new_path = path.join(process.env.PWD, "/temp/", file_name + "." + file_ext);
        }
        console.log(new_path);
        // No files
        if (!file_size) return res.status(400).send("No files were uploaded - file size 0");
        const folders = new_path.split(path.sep).slice(0, -1)
        if (folders.length) {
            // create folder path if it doesn't exist
            folders.reduce((last, folder) => {
            const folderPath = last ? last + path.sep + folder : folder
            if (!fs.existsSync(folderPath)) {
                fs.mkdirSync(folderPath)
            }
            return folderPath
            });
        }
        const data = fs.readFileSync(old_path);
        fs.writeFileSync(new_path, data);
        fs.unlinkSync(old_path);
        // upload to oos and convert to svf
        const token = await twoLeggedAuthenticationNew(true).access_token;
        var params = { fileName: file_name+"."+file_ext , token: token,name:result.fields.name };
        let resultOfUploadFileToBucket = await uploadFileToBucket(params);
        //parmas = { objectId: resultOfUploadFileToBucket.data.objectId, token: token };
        //const resultOfModelDerivativeToSVF = await modelDerivativeToSVF(parmas);
        // return encodeProjectId and theToken
        const buff = new Buffer(resultOfUploadFileToBucket.objectId);
        const encodeProjectId = buff.toString("base64");
        const theToken = await getTokenForViewer();
        retObject = { token: theToken, objectId: encodeProjectId};
        res.status(200);
        res.json(retObject);
    } catch (ex) {
        console.log(ex);
        res.status(500);
        res.json({ message: ex });
    }
});

// getBucketObjects
router.get("/getBucketObjects/:bucketName", async (req, res) => {
    if (!req.params.bucketName) return res.status(400).send("Invaild parameters");

    // console.log(req.params.bucketName);
    let bucketObjects = await getBucketObjects(req.params.bucketName);
    // console.log("---bucketObjects---", bucketObjects);
    res.send(bucketObjects);
});
router.get('/api/models', async function (req, res, next) {
    try {
        const objects = await APS.listObjects();
        res.json(objects.map(o => ({
            name: o.objectKey,
            urn: urnify(o.objectId)
        })));
    } catch (err) {
        next(err);
    }
});
router.get("/getAutodeskToken", async (req, res) => {
    let token = await twoLeggedAuthenticationNew(false).access_token;
    res.send({ token: token });
});

router.get("/verifyJobComplete/:urn/status", async (req, res) => {
    try {
        if (!req.params.urn) return res.status(400).send("Invaild parameters");
        //let result = await getDesignDataMainfest(req.params.urn);
        const manifest = await APS.getManifest(req.params.urn);
        if (manifest) {
            let messages = [];
            if (manifest.derivatives) {
                for (const derivative of manifest.derivatives) {
                    messages = messages.concat(derivative.messages || []);
                    if (derivative.children) {
                        for (const child of derivative.children) {
                            messages.concat(child.messages || []);
                        }
                    }
                }
            }
            res.json({ status: manifest.status, progress: manifest.progress, messages });
        } else {
            res.json({ status: 'n/a' });
        }
        //res.send(result.data);
    } catch (ex) {
        console.log(ex);
        res.status(500);
        res.json({ message: ex });
    }
});

async function getDesignDataMainfest(urn) {
    let forge = config.get("forge").oauth;
    let url = forge.baseUrl + "/modelderivative/v2/designdata/" + urn + "/manifest";
    console.log("url", url);
    try {
        let token = await twoLeggedAuthenticationNew(true).access_token;
        let headers = { headers: { Authorization: "Bearer " + token } };
        var result = await axios.get(url, headers);
        return result;
    } catch (ex) {
        return ex.message;
    }
}

async function getBucketObjects(bucketName) {
    console.log("----bucketName---", bucketName);
    let token = await twoLeggedAuthenticationNew(false).access_token;
    let forge = config.get("forge").oauth;
    let url = forge.baseUrl + "/oss/v2/buckets/" + bucketName + "/objects?limit=100";
    let headers = { headers: { Authorization: "Bearer " + token } };
    return axios
        .get(url, headers)
        .then(function(response) {
            return response.data;
        })
        .catch(function(error) {
            console.log("getBucketObjects error");
            return "getBucketObjects error";
        });
}
function twoLeggedAuthentication() {
    let forge = config.get("forge").oauth;
    let url = forge.baseUrl + forge.authenticationUrl;
    let headers = { headers: { "Content-Type": "application/x-www-form-urlencoded" } };
    let data = querystring.stringify({
        client_id: forge.clientId,
        client_secret: forge.clientSecret,
        grant_type: "client_credentials",
        scope: forge.scope
    });
    return axios
        .post(url, data, headers)
        .then(function(response) {
            // console.log(response.data.access_token);
            return response.data.access_token;
        })
        .catch(function(error) {
            // console.log(error);
            console.log("getToken error");
            return error.message;
        });
}
async function  twoLeggedAuthenticationNew(internal) {
    
    if(internal)
        return APS.getInternalToken();
    else
        return APS.getPublicToken();
}

function getTokenForViewer() {
    // let forge = config.get("forge").oauth;
    // let url = forge.baseUrl + forge.authenticationUrl;
    // let headers = { headers: { "Content-Type": "application/x-www-form-urlencoded" } };
    // let data = querystring.stringify({
    //     client_id: forge.clientId,
    //     client_secret: forge.clientSecret,
    //     grant_type: "client_credentials",
    //     scope: "data:read"
    // });
    // return axios
    //     .post(url, data, headers)
    //     .then(function(response) {
    //         //console.log('getTokenForViewer', response.data.access_token);
    //         return response.data.access_token;
    //     })
    //     .catch(function(error) {
    //         console.log("getToken error");
    //         return error.message;
    //     });
    return twoLeggedAuthenticationNew(true);
}

function deleteFilesOlder30mins() {
    var uploadsDir = path.join(process.env.PWD, "/temp/");
    fs.readdir(uploadsDir, function(err, files) {
        files.forEach(function(file, index) {
            fs.stat(path.join(uploadsDir, file), function(err, stat) {
                var endTime, now;
                if (err) {
                    return console.error(err);
                }
                now = new Date().getTime();
                endTime = new Date(stat.ctime).getTime() + 1800000;
                if (now > endTime) {
                    return fs.unlink(path.join(uploadsDir, file), function(err) {
                        if (err) {
                            return console.error(err);
                        }
                        console.log("successfully deleted");
                    });
                }
            });
        });
    });
}

async function uploadFileToBucket(params) {
    let file_dir;
    if (process.platform === "win32") {
        fileDir = path.join(process.cwd(), "\\temp\\" + params.fileName);
    } else {
        fileDir = path.join(process.env.PWD, "/temp/" + params.fileName);
    }
    // let config = {
    //     onUploadProgress: function(progressEvent) {
    //         var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
    //     },
    //     headers: {
    //         Authorization: "Bearer " + params.token,
    //         "Content-Type": "application/octet-stream"
    //         // "Content-Length": "52428890"
    //     }
    // };
    // let url = "https://developer.api.autodesk.com/oss/v2/buckets/" + params.bucketName + "/objects/" + params.fileName;
    // console.log(config);
    // return new Promise(async (resolve, reject) => {
    //     try {
    //         var fileData = await mzfs.readFile(fileDir);
    //         // console.log('uploadFileToBucket fileData', fileData);
    //         var result = await axios.put(url, fileData, config);
    //         // console.log('uploadFileToBucket axios.put', result);
    //         fs.unlinkSync(fileDir);
    //         resolve(result);
    //     } catch (ex) {
    //         console.log("uploadFileToBucket reject", ex.message);
    //         reject(ex.message);
    //     }
    // });
    const file = fileDir;
    if (!params.fileName) {
        res.status(400).send('The required field ("model-file") is missing.');
        return;
    }
    try {
        const obj = await APS.uploadObject(params.name, file);
        await APS.translateObject(APS.urnify(obj.objectId));
        return obj;
    } catch (err) {
        next(err);
    }
}

async function modelDerivativeToSVF(params) {
    // let token = await twoLeggedAuthentication();
    var url = "https://developer.api.autodesk.com/modelderivative/v2/designdata/job";
    var headers = { headers: { Authorization: "Bearer " + params.token, "Content-Type": "application/json" } };
    var buff = new Buffer(params.objectId);
    var base64data = buff.toString("base64");
    var data = {
        input: {
            urn: base64data
        },
        output: {
            formats: [
                {
                    type: "svf",
                    views: ["2d", "3d"]
                }
            ]
        }
    };
    return new Promise(async (resolve, reject) => {
        try {
            var result = await axios.post(url, data, headers);
            resolve(result);
        } catch (ex) {
            console.log("modelDerivativeToSVF reject", ex.message);
            return reject(ex.message);
        }
    });
}

function readFilesFormPromise(req) {
    return new Promise(async (resolve, reject) => {
        try {
            var form = new formidable.IncomingForm();
            form.parse(req, function(err, fields, files) {
                // console.log('readFilesFormPromise resolve', { err: err, fields: fields, files: files });
                resolve({ err: err, fields: fields, files: files });
            });
        } catch (ex) {
            console.log("readFilesFormPromise reject", ex.message);
            return reject(ex.message);
        }
    });
}

async function readFilePromise() {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, (err, data) => {
            if (err) reject(err);
            resolve(data);
        });
    });
}

module.exports = router;
