const express = require("express");
const router = express.Router();
const Joi = require("joi");
const markupService = require('./../services/markup');
const schema = Joi.object().keys({
    markup: Joi.string().required().error(new Error('Give your error message here for markup')),
    viewer_state: Joi.string().required().error(new Error('Give your error message here for viewer_state')),
    model_id: Joi.string().required().error(new Error('Give your error message here for model_id')),
  
})//.with('username', 'birthyear').without('password', 'access_token');

router.get(['/','/getByModelId/:markup_id'], async (req, res) => {
    try {
        const ret = await markupService.getMarkup(req.params.markup_id);
        res.json(ret);
    } catch (ex) {
        console.log(ex);
        res.status(500);
        res.json({ message: ex });
    }
});

router.post("/", async (req, res) => {
    try {
        await Joi.validate(req.body, schema, function (err, value) { 
            if(err) {
                res.status(400);
                res.send(err.message);
            }
        });
        const ret = await markupService.saveMarkup(req.body.markup, req.body.viewer_state,  req.body.model_id);
        res.send(ret);
    } catch (ex) {
        console.log(ex);
        res.status(500);
        res.json({ message: ex });
    }   
});

router.get("/create", async (req, res) => {
    try {
        const ret = await markupService.createTable();
        res.json(ret);
    } catch (ex) {
        console.log(ex);
        res.status(500);
        res.json({ message: ex });
    }
});

module.exports = router;