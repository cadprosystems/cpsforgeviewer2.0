"use strict";
const config = require("config");
const path = require("path");
const express = require("express");
const exphbs = require("express-handlebars");// view engine
const followRedirects = require('follow-redirects');
followRedirects.maxRedirects = 10;
followRedirects.maxBodyLength = 500 * 1024 * 1024 * 1024; // 500 GB
const hbs = exphbs.create({
    // Specify helpers which are only registered on this instance.
    // helpers: {
    //     foo: function () { return 'FOO!'; },
    //     bar: function () { return 'BAR!'; }
    // }
});

// Constants
const PORT = 8080;
const HOST = "0.0.0.0";

console.log("Applicaion Name: " + config.get("name"));

// App
const app = express();
app.engine("handlebars", hbs.engine);
app.set("view engine", "handlebars");
app.set("views", path.join(__dirname, "/webserver/views"));

require("./webserver/routers")(app);
require("./apiserver/index")(app);
app.use(express.static("public"));

app.listen(PORT, HOST);

console.log(`Running on http://${HOST}:${PORT}`);

